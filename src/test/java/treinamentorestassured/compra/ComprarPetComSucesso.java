package treinamentorestassured.compra;

import org.junit.Before;
import org.testng.annotations.Test;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;




import static io.restassured.RestAssured.given;

public class ComprarPetComSucesso {


    //criar um pet antes do teste

    @Test
    public void comprarPetComSucesso(){

        JSONObject pet = new JSONObject();
        pet.put("id", 0);
        pet.put("petId", 99998);
        pet.put("quantity", 1);
        pet.put("shipDate", "2022-03-28T16:12:16.226Z");
        pet.put("status", "placed");
        pet.put("complete", true);



        given()
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/store/order")
                .header("content-type", "application/json")
                .body(pet)
        .when()
                .post()
        .then()
                .statusCode(200);

    }
}
