package treinamentorestassured.atualizar.objects;

public class Category {
    private long id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getID() {
        return id;
    }

    public void setID(long value) {
        this.id = value;
    }
}