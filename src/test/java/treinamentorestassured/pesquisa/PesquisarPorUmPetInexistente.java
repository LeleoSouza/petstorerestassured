package treinamentorestassured.pesquisa;

import org.junit.Test;

import static io.restassured.RestAssured.given;

public class PesquisarPorUmPetInexistente {

    //criar um pet antes do teste e deletá-lo

    @Test
    public void pesquisarPorPetInexistente(){

        given()
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/pet/{petId}")
                .pathParam("petId", 99999)
        .when()
                .get()
        .then()
                .statusCode(404);
    }
}
