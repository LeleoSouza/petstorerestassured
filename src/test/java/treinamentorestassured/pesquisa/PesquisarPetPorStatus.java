package treinamentorestassured.pesquisa;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class PesquisarPetPorStatus {


    //cadastrar um pet antes com status "pending"

    @Test
    public void pesquisarPetPorStatusPendingComSucesso(){

        given()
                .baseUri("https://petstore.swagger.io/v2")
                .basePath("/pet/findByStatus")
                .queryParam("status", "pending")
                .header("content-type", "application/json")
        .when()
                .get()
        .then()
                .statusCode(200);

    }

}
