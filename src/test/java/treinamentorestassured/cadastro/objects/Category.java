package treinamentorestassured.cadastro.objects;

public class Category {

    public int id;
    public String name;

    public int getId() {
        return id;
    }

    public void setID(int id) {this.id = id;}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
