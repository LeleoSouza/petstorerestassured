package treinamentorestassured.cadastro;

import org.testng.annotations.Test;
import treinamentorestassured.cadastro.objects.Category;
import treinamentorestassured.cadastro.objects.Pet;
import treinamentorestassured.cadastro.objects.Tags;

import static io.restassured.RestAssured.*;

public class CadastroComSucesso {

    @Test
    public void cadastrarPetComSucesso(){
        Pet pet = new Pet();
        pet.setID(99998);

        Category category = new Category();
        category.setID(99998);
        category.setName("Soluco");
        pet.setCategory(category);

        pet.setName("Shepherd");
        pet.setPhotoUrls(new String[]{"http://fotosdegato.com.br/foto1.png", "http://fotosdegato.com.br/foto2.png"});

        Tags tag1 = new Tags();
        tag1.setID(99998);
        tag1.setName("Sem raca definida");
        Tags tag2 = new Tags();
        tag2.setID(99999);
        tag2.setName("Amarelo");
        pet.setTags(new Tags[]{tag1, tag2});

        pet.setStatus("available");

    given()
            .baseUri("https://petstore.swagger.io/v2")
            .basePath("/pet")
            .header("content-type", "application/json")
            .body(pet).
    when()
            .post()
    .then()
            .statusCode(200);





    }
}
